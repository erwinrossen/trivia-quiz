from django.test import TestCase

from question.models import Question, Answer, Topic


class QuestionTestCase(TestCase):
    def test_question_to_dict(self):
        topic = Topic.objects.create(code='geography', name='Geography')
        question = Question.objects.create(topic=topic, code='capital-france',
                                           question_text='What is the capital of France?')
        Answer.objects.bulk_create([
            Answer(question=question, answer_text='Paris', is_correct=True,
                   explanation='Paris is the capital of France'),
            Answer(question=question, answer_text='London', is_correct=False,
                   explanation='London is the capital of England'),
            Answer(question=question, answer_text='Berlin', is_correct=False,
                   explanation='Berlin is the capital of Germany'),
            Answer(question=question, answer_text='Amsterdam', is_correct=False,
                   explanation='Amsterdam is the capital of The Netherlands'),
        ])

        question_dict = question.to_dict()
        expected_dict = {
            'code': 'capital-france',
            'topic': topic.code,
            'question_text': 'What is the capital of France?',
            'answers': [
                {'answer_text': 'Paris', 'is_correct': True,
                 'explanation': 'Paris is the capital of France', },
                {'answer_text': 'London', 'is_correct': False,
                 'explanation': 'London is the capital of England'},
                {'answer_text': 'Berlin', 'is_correct': False,
                 'explanation': 'Berlin is the capital of Germany'},
                {'answer_text': 'Amsterdam', 'is_correct': False,
                 'explanation': 'Amsterdam is the capital of The Netherlands'}
            ],
        }
        self.assertDictEqual(expected_dict, question_dict)

    def test_questions_from_dicts(self):
        topic = Topic.objects.create(code='geography', name='Geography')

        question_dicts = [
            {
                'code': 'capital-france',
                'topic': topic.code,
                'question_text': 'What is the capital of France?',
                'answers': [
                    {'answer_text': 'Paris', 'is_correct': True,
                     'explanation': 'Paris is the capital of France', },
                    {'answer_text': 'London', 'is_correct': False,
                     'explanation': 'London is the capital of England'},
                    {'answer_text': 'Berlin', 'is_correct': False,
                     'explanation': 'Berlin is the capital of Germany'},
                    {'answer_text': 'Amsterdam', 'is_correct': False,
                     'explanation': 'Amsterdam is the capital of The Netherlands'}
                ],
            },
            {
                'code': 'capital-england',
                'topic': 'english-geography',
                'question_text': 'What is the capital of England?',
                'answers': [
                    {'answer_text': 'London', 'is_correct': True,
                     'explanation': 'London is the capital of England'},
                    {'answer_text': 'Paris', 'is_correct': False,
                     'explanation': 'Paris is the capital of France', },
                    {'answer_text': 'Berlin', 'is_correct': False,
                     'explanation': 'Berlin is the capital of Germany'},
                    {'answer_text': 'Amsterdam', 'is_correct': False,
                     'explanation': 'Amsterdam is the capital of The Netherlands'}
                ],
            }
        ]

        # We expect 3 queries to be executed:
        # - 1 to check which questions already exist
        # - 1 to check which topics already exist
        # - 1 to create additional topics
        # - 1 to create the questions in bulk
        # - 1 to create the answers in bulk
        with self.assertNumQueries(5):
            questions = Question.from_dicts(question_dicts)

        # Verify that the all topics, questions and answers are created
        self.assertEqual(2, len(questions))
        self.assertEqual(2, Topic.objects.count())
        self.assertEqual(2, Question.objects.count())
        self.assertEqual(8, Answer.objects.count())

        # Verify that questions and answers are properly created
        q1 = Question.objects.get(code='capital-france')
        self.assertEqual('Geography', q1.topic.name)
        self.assertEqual('What is the capital of France?', q1.question_text)
        self.assertEqual(4, q1.answers.count())
        self.assertEqual('Paris', q1.correct_answer.answer_text)

        # Verify that topics are properly created
        q2 = Question.objects.get(code='capital-england')
        self.assertEqual('English Geography', q2.topic.name)
