import json
import os

from django.conf import settings

from question.models import Question

QUESTION_DIR = settings.BASE_DIR / 'data' / 'questions'


def export_questions():
    """
    Export all questions in the database to JSON files
    """

    questions = Question.objects.all().iterator()
    for question in questions:
        filename = f'{question.code}.json'
        filepath = QUESTION_DIR / filename
        with open(filepath, 'w') as f:
            json.dump(question.to_dict(), f, indent=4)


def import_questions():
    """
    Import all questions from JSON files into the database
    """

    question_files = os.scandir(QUESTION_DIR)
    question_dicts = []
    for file_name in question_files:
        with open(file_name) as f:
            question_dicts.append(json.load(f))
    Question.from_dicts(question_dicts)
