from django.core.management import BaseCommand

from question.io import export_questions


class Command(BaseCommand):
    help = 'Export all questions in the database to JSON files'

    def handle(self, *args, **options):
        export_questions()
