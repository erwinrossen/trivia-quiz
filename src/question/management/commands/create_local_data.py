from django.conf import settings
from django.contrib.auth.models import User
from django.core.management import BaseCommand, call_command

from question.io import import_questions
from question.models import Topic


class Command(BaseCommand):
    help = 'Fill the local database with prepopulated data from the data directory'

    def handle(self, *args, **options):
        if settings.ENVIRONMENT != 'local':
            msg = 'Cannot create local data on a non-local environment'
            raise AssertionError(msg)

        call_command('migrate')
        if not User.objects.filter(is_staff=True).exists():
            User.objects.create_superuser(username='admin', email='admin@example.com', password='admin')
            print('Create superuser with username "admin" and password "admin"')

        # Load all questions from the questions directory
        import_questions()
