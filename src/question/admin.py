from django.contrib import admin
from django.contrib.auth.models import Group, User
from django.db.models import Count

from question.models import Answer, Question, Topic

# This should be done in another place, e.g. in an app called `common`,
# but since we only have one app so far, this is considered overkill.
admin.site.unregister(Group)
admin.site.unregister(User)


class TopicFilter(admin.SimpleListFilter):
    title = 'topic'
    parameter_name = 'topic'

    def lookups(self, request, model_admin):
        return Topic.objects.values_list('code', 'name')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(topic_id=self.value())
        else:
            return queryset


class QuestionInline(admin.TabularInline):
    model = Question
    extra = 0

    def has_add_permission(self, request, obj):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 0

    ordering = ('-is_correct',)  # Ensure that the correct answer is on top


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'question_count',)
    search_fields = ('name',)
    fields = ('code', 'name', 'created_at', 'updated_at', 'question_count',)
    readonly_fields = ('created_at', 'updated_at', 'question_count',)
    inlines = (QuestionInline,)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate(question_count=Count('questions'))
        return qs

    def question_count(self, obj: Topic) -> int:
        return obj.question_count


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('code', 'question_text', 'topic',)
    search_fields = ('question_text',)
    list_filter = (TopicFilter,)

    fields = ('topic', 'question_text', 'created_at', 'updated_at')
    readonly_fields = ('created_at', 'updated_at')
    inlines = (AnswerInline,)
