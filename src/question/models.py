from functools import cached_property

import itertools
from typing import List, TypedDict

from django.db import models


class AnswerDict(TypedDict):
    answer_text: str
    is_correct: bool
    explanation: str


class QuestionDict(TypedDict):
    code: str
    topic: str
    question_text: str
    answers: List[AnswerDict]


class Topic(models.Model):
    code = models.CharField(max_length=255, primary_key=True)
    name = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @cached_property
    def question_count(self) -> int:
        return self.questions.count()

    def __str__(self):
        return self.name


class Question(models.Model):
    code = models.CharField(max_length=255, unique=True)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE, related_name='questions')
    question_text = models.CharField(max_length=255)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.question_text

    def to_dict(self) -> QuestionDict:
        return {
            'code': self.code,
            'topic': str(self.topic_id),
            'question_text': self.question_text,
            'answers': [answer.to_dict() for answer in self.answers.all()]
        }

    @staticmethod
    def from_dicts(question_dicts: List[QuestionDict]):
        """
        Create questions and their answers in the database from a list of dictionaries

        Note: We only create new questions and answers. If the question already exists, we do not update it.

        :param question_dicts: The list of question dictionaries
        :return: The created questions
        """

        existing_question_codes = set(Question.objects.values_list('code', flat=True))
        existing_topic_codes = set(Topic.objects.values_list('code', flat=True))

        result = [Question.from_dict(question_dict) for question_dict in question_dicts]
        questions = [r[0] for r in result if r[0].code not in existing_question_codes]
        answers = list(itertools.chain(*[r[1] for r in result if r[0].code not in existing_question_codes]))

        topics = set([q.topic_id for q in questions])
        Topic.objects.bulk_create([Topic(code=topic, name=topic.replace('-', ' ').title())
                                   for topic in topics
                                   if topic not in existing_topic_codes])

        Question.objects.bulk_create(questions)
        Answer.objects.bulk_create(answers)
        return questions

    @staticmethod
    def from_dict(question_dict: QuestionDict) -> ('Question', List['Answer']):
        """
        Create an in-memory question and its answers from a dictionary

        :param question_dict: The question dictionary
        :return: The question and its answers
        """

        question = Question(code=question_dict['code'],
                            topic_id=question_dict['topic'],
                            question_text=question_dict['question_text'])
        answers = [Answer.from_dict(question, answer_dict) for answer_dict in question_dict['answers']]
        return question, answers

    @property
    def correct_answer(self) -> 'Answer':
        return self.answers.get(is_correct=True)

    @property
    def incorrect_answers(self) -> List['Answer']:
        return self.answers.filter(is_correct=False)


class Answer(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')
    answer_text = models.CharField(max_length=255)
    is_correct = models.BooleanField()
    explanation = models.TextField()

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.answer_text

    class Meta:
        verbose_name_plural = 'Multiple choice answers'

        # Only one multiple choice answer can be correct per question, but there can be multiple incorrect answers
        constraints = [
            models.UniqueConstraint(fields=['question', 'is_correct'], condition=models.Q(is_correct=True),
                                    name='unique_correct_answer_per_question'),
        ]

    def to_dict(self) -> AnswerDict:
        return {
            'answer_text': self.answer_text,
            'is_correct': self.is_correct,
            'explanation': self.explanation,
        }

    @staticmethod
    def from_dict(question: Question, answer_dict: AnswerDict) -> 'Answer':
        """
        Create an in-memory answer from a dictionary

        :param question: The question the answer belongs to
        :param answer_dict: The answer dictionary
        :return: The in-memory Answer
        """

        return Answer(question=question, **answer_dict)
