# Trivia quiz

A question-and-answer game that tests a player's knowledge about a multitude of topics.

### Question generation

ChatGPT can help generating questions and answers, but it needs specific prompts for good results. Try this one.

```
I have questions and answers in the following format. Each question has six answers, of which only 1 answer is correct. 
The other answers are plausible. Each answer has an explanation why it is correct, or why it is plausible but incorrect. 
The questions come in a JSON format, of which these are some examples.

{
    "code": "asteroid-belt-mars-jupiter",
    "topic": "astronomy",
    "question_text": "What is the name of the asteroid belt located between Mars and Jupiter?",
    "answers": [
        {
            "answer_text": "Main Asteroid Belt",
            "is_correct": true,
            "explanation": "The Main Asteroid Belt is a region of space located between the orbits of Mars and Jupiter, containing numerous small bodies called asteroids."
        },
        {
            "answer_text": "Kuiper Belt",
            "is_correct": false,
            "explanation": "This is a region of the solar system beyond the orbit of Neptune that contains many small, icy bodies. It sounds plausible because it is another region of the solar system that is home to many small objects, but it is located much farther out than the asteroid belt."
        },
        {
            "answer_text": "Oort Cloud",
            "is_correct": false,
            "explanation": "This is a hypothesized region of the solar system that is thought to contain a large number of icy objects, including comets, at the outer edges of the solar system. While it is another region of the solar system that contains many small objects, it is not located between Mars and Jupiter."
        },
        {
            "answer_text": "Saturn Belt",
            "is_correct": false,
            "explanation": "Saturn is a gas giant planet in the outer solar system, and it has its own set of rings composed of small icy and rocky particles. While it is a belt of sorts, it is not the asteroid belt located between Mars and Jupiter."
        },
        {
            "answer_text": "Asteroid Zone",
            "is_correct": false,
            "explanation": "This term sounds plausible because it uses the word \"asteroid,\" which is a term commonly associated with the asteroid belt. However, it is not the official name of the belt located between Mars and Jupiter."
        },
        {
            "answer_text": "Middle Belt",
            "is_correct": false,
            "explanation": "While this term sounds plausible because it is using the word \"belt,\" it is not a commonly used term to refer to the asteroid belt."
        }
    ]
}

Give me 6 question and answers in this JSON format about physics. 
```